# <od-cabinet>

> A component to house expanding content.

`<od-cabinet>` is a simple component that can control the behaviour of `<od-cabinet-items>`. The content expands into view and can be configured to only ever display one pice of content at a time.

## Installation
- Install with [npm](https://www.npmjs.com/)

```
npm i @orcden/od-cabinet
```
## Usage
```
import '@orcden/od-cabinet';
```
```
<od-cabinet hold-open>
    <od-cabinet-item>
        <span slot='title'>Testing</span>
        <p>
            Test 1
        </p>
    </od-cabinet-item>

    <od-cabinet-item>
        <span slot='title'>Testing2</span>
        <p>
            Test 2
        </p>
    </od-cabinet-item>

    <od-cabinet-item>
        <span slot='title'>Testing3</span>
        <p>
            Test 3
        </p>
    </od-cabinet-item>
</od-cabinet>
```
    
## Attributes
### OD-Cabinet
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `closed`  | Boolean  | true      | Get. You can set this to true to close all items. otherwise does nothing  |
| `hold-open`  | Boolean | false   | Set this to true so that the gallery doesn't automatically close other cabinet items |

### OD-Cabinet-Item
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `open`  | Boolean | false   | Use this property to show or hide the content in the cabinet item |

## Properties
### OD-Cabinet
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `closed`  | Boolean  | true      | Get. You can set this to true to close all items. otherwise does nothing  |
| `holdOpen`  | Boolean | false   | Set this to true so that the gallery doesn't automatically close other cabinet items |

### OD-Cabinet-Item
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `open`  | Boolean | false   | Use this property to show or hide the content in the cabinet item |

## Functions
### OD-Cabinet
| Name | Parameters | Description                                  |
|-----------|------|-----------------------------------------------|
| `closeItems`   | None | Closes all the child cabinet items |

### OD-Cabinet-Item
| Name | Parameters | Description                                  |
|-----------|------|-----------------------------------------------|
| `toggleOpen`   | None | Can be used to open and close the cabinet item content |

## Styling
- CSS variables are available to alter the default styling provided

### OD-Cabinet
| Shadow Parts     | Description           |
|------------------|-----------------------|
| cabinet           | The div used to house the cabinet items |

### OD-Cabinet-Item
| Shadow Parts     | Description           |
|------------------|-----------------------|
| title           | The div used to house the title slot |
| sliding-container | A div used to transition the content. Change 'transition' to adjust timings |
| sliding-item      | A div used to transition the content. |
| content         | The div used to house the actual cabinet item content. |

## Development
### Run development server and show demo

```
npm run demo
```

### Run linter

```
npm run lint
```

### Fix linter errors

```
npm run fix
```

### Run tests

```
npm run test
```

### Build for production

```
npm run build
```