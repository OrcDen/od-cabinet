const template = document.createElement( "template" );
template.innerHTML = `
    <style>
        :host {
            display: flex;
            flex-direction: column;
        }

        #title-container {
            cursor: pointer;
            background: green;
            border: 1px solid black;
        }

        #title-container:hover {
            background: limegreen;
        }

        #sliding-container {
            position: relative;
            overflow: hidden;

            transition: all .5s ease-in-out;
        }

        #item-container {
            width: 100%;
            position: absolute;
            bottom: 0;
        }

        #content-container {
            width: 100%;
            border: 1px solid black;
            border-top: 0;
            margin: 0 1em;
        }

        .container {
            display: flex;
            align-items: center;
            justify-content: center;
        }
        
    </style>

    
    <div id="title-container" class="container" part="title">
        <slot name="title" id="title-slot"></slot>
    </div>
    <div id="sliding-container" part="sliding-container">
        <div id="item-container" class="container" part="sliding-item">
            <div id='content-container' class="container" part='content'>
                <slot id="item-slot"></slot>
            </div>
        </div>
    </div>
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-cabinet-item" );

export class OdCabinetItem extends HTMLElement {
    constructor() {
        super();
        //constructor creates the shadowRoot
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    connectedCallback() {
        //set attribute default values first

        //all properties should be upgraded to allow lazy functionality
        this._upgradeProperty( "open" );

        //listeners and others etc.
        let title = this.shadowRoot.querySelector( "#title-container" );
        title.addEventListener( "click", () => {
            this.toggleOpen();
        } );

        let resizeObserver = new ResizeObserver( () => {
            if( this.open ) {
                this._openCabinet();
            }
        } );
        resizeObserver.observe( this.shadowRoot.querySelector( "#content-container" ) );
    }

    disconnectedCallback() {}
      

    //General - every element should have this
    _upgradeProperty( prop ) {
        if ( Object.prototype.hasOwnProperty.call( this, prop ) ) {
            var value = this[prop];
            delete this[prop];
            this[prop] = value;
        }
    }

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return ["open"];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        switch ( attrName ) {
            case "open": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setOpenAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setOpenAttribute( newValue );
                    if( newValue === 'true' ) {
                        this._openCabinet();
                        this.dispatchEvent( new CustomEvent( 'od-cabinet-item-open', {bubbles: true} ) );
                    } else {
                        this._closeCabinet();
                        this.dispatchEvent( new CustomEvent( 'od-cabinet-item-close', {bubbles: true} ) );
                    }
                }
                break;
            }
        }
    }

    //properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    get open() {
        return this.hasAttribute( "open" );
    }

    set open( isOpen ) {
        if ( typeof isOpen !== "boolean" ) {
            return;
        }
        this._setOpenAttribute( isOpen );
    }

    _setOpenAttribute( newV ) {        
        this._setBooleanAttribute( 'open', newV );
    }

    _validateBoolean( newV ) {
        return (
            typeof newV === "boolean" || newV === "true" || newV === "false"
        );
    }

    _setBooleanAttribute( name, newV ) {
        if ( newV !== "" && ( !newV || newV === "false" ) ) {
            this.removeAttribute( name );
        } else {
            this.setAttribute( name, true );
        }
    }
    
    _openCabinet() {
        let height = this.shadowRoot.querySelector( '#item-container' ).offsetHeight;
        this._transition( height );
    }

    _closeCabinet() {
        this._transition( 0 );
    }

    _transition( height ) {
        let container = this.shadowRoot.querySelector( '#sliding-container' );
        let origHeight = container.offsetHeight;
        let transition = container.style.transition;
        container.style.transition = '';
        
        requestAnimationFrame( function() {
            container.style.height = origHeight + 'px';
            container.style.transition = transition;

            requestAnimationFrame( function() {
                container.style.height = height + 'px';
            });
        });
    }

    //Public
    toggleOpen() {
        this.open = !this.open;
    }
}
