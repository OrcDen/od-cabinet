const template = document.createElement( "template" );
template.innerHTML = `
     <style>
        :host{
            display: flex;
            width: 100%;
        }

        #cabinet-container {
            display: flex;
            flex-direction: column;
            justify-content: center;
            width: 100%;
            height: 100%;

            border: 1px solid black;
        }
    </style>
    
    <div id="cabinet-container" part='cabinet'>
        <slot id="item-slot"></slot>
    </div>
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-cabinet" );

export class OdCabinet extends HTMLElement {
    constructor() {
        super();
        //constructor creates the shadowRoot
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    connectedCallback() {
        this._openItems = [];
        if ( !this.hasAttribute( "closed" ) ) {
            this.setAttribute( "closed", true );
        }
        this._upgradeProperty( "closed" );
        this._upgradeProperty( "holdOpen" );

        this.addEventListener( 'od-cabinet-item-open', ( e ) => { this._setOpen( e ) } );
        this.addEventListener( 'od-cabinet-item-close', ( e ) => { this._setClosed( e ) } );
        this.closeItems();
    }

    disconnectedCallback() {}

    //General - every element should have this
    _upgradeProperty( prop ) {
        if ( Object.prototype.hasOwnProperty.call( this, prop ) ) {
            var value = this[prop];
            delete this[prop];
            this[prop] = value;
        }
    }

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return ["closed", "hold-open"];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        switch ( attrName ) {
            case "closed": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setClosedAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {                    
                    this._setClosedAttribute( newValue );
                    if( newValue === 'true' ) {
                        this.closeItems();
                    }
                }
                break;
            }
            case "hold-open": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setHoldAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setHoldAttribute( newValue );
                }
                break;
            }
        }
    }

    //properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    get closed() {
        return this.hasAttribute( "closed" );
    }

    set closed( isClosed ) {
        if ( typeof isClosed !== "boolean" ) {
            return;
        }
        this._setClosedAttribute( isClosed );
    }

    _setClosedAttribute( newV ) {
        this._setBooleanAttribute( 'closed', newV );
    }

    _validateBoolean( newV ) {
        return (
            typeof newV === "boolean" || newV === "true" || newV === "false"
        );
    }

    _setBooleanAttribute( name, newV ) {
        if ( newV !== "" && ( !newV || newV === "false" ) ) {
            this.removeAttribute( name );
        } else {
            this.setAttribute( name, true );
        }
    }

    get holdOpen() {
        return this.hasAttribute( "hold-open" );
    }

    set holdOpen( holdOpen ) {
        if ( typeof holdOpen !== "boolean" ) {
            return;
        }
        this._setHoldAttribute( holdOpen );
    }

    _setHoldAttribute( newV ) {        
        this._setBooleanAttribute( 'hold-open', newV );
    }

    _setOpen( e ) {
        if ( !this.closed ) {
            if ( !this.holdOpen ) {
                this.closeItems();
            }
        }
        this._openItems.push( e.target );
        this.closed = false;
    }

    _setClosed( e ) {
        this._openItems.splice( this._openItems.indexOf( e ), 1 );
        if( this._openItems.length < 1 ) {
            this.closed = true;
        }
    }

    closeItems() {
        for( var i in this._openItems ) {
            var item = this._openItems[i];
            item.open = false;
        }
    }
}
